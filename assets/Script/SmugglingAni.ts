import BezierEx from "./BezierEx";

const {ccclass, property, requireComponent} = cc._decorator;

@ccclass
export default class SmugglingAni extends cc.Component {
    @property(BezierEx)
    bezierEx:BezierEx = null;

    @property(cc.Node)
    roleNode = null;


    onLoad () {
        // init logic
        this.bezierEx = this.bezierEx || this.node.getComponent(BezierEx);
    }

    onDrawClick(target, event){
        console.log('onBtnClick');

        this.bezierEx.drawLines();

        // this.roleNode.setPosition(0, 0);

        // const param = [cc.v2(this.roleNode.x, this.roleNode.y),
        //     cc.v2(this.p1.x, this.p1.y), 
        //         cc.v2(this.p2.x, this.p2.y)];
        // const act = cc.bezierTo(3, param);
        // this.roleNode.runAction(act);
    }

    async onActClick(){
        const bezierData = this.bezierEx.bezierData;
        this.roleNode.setPosition(bezierData[0][0].x, bezierData[0][0].y);

        for(const v of bezierData){
            await new Promise<void>((resolve)=>{
                const dt = cc.Vec2.distance(v[0], v[2]) / 100;
                const bezie = cc.bezierTo(dt, v);
                const act = cc.sequence(bezie, cc.delayTime(1), cc.callFunc(()=>{resolve()}))
                this.roleNode.runAction(act);
            })
        }
    }

    onclearClick(){
        this.bezierEx.clear();
    }
    
}
